appId = 'ComuneRovereto';
APP_VERSION = '0.1.0';
cityName = {
    'it': 'Rovereto Percorsi',
    'en': 'Rovereto Paths',
    'de': 'Rovereto Paths'
};
QUESTIONNAIRE = true;
EXTLOGGING = true;

FILTERSOCIALSLIDE = true;
FILTERSOCIALTAB = true;
FILTERSOCIALADDIMAGE = true;
FILTERMAXNUMBERSLIDE = 6;

FORCED_LANGUAGE = 'it';

SCHEMA_VERSION = 3;
